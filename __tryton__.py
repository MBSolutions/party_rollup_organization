#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
{
    'name': 'Party Rollup Organization',
    'name_de_DE': 'Partei Struktur Organisation',
    'version': '2.2.0',
    'author': 'virtual-things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''Organization Rollup
    - Provides the ability to build hierarchical organisation structures
    ''',
    'description_de_DE': '''Strukturierung Organisationen
    - Ermöglicht den Aufbau von hierarchischen Organisationsstrukturen
''',
    'depends': [
        'party_type',
        'party_rollup',
    ],
    'xml': [
        'party.xml',
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
